# Obtain an SSL/TLS certificate from Let's Encrypt
certbot certonly --domain ghastbot.gitlab.io

# Add your SSL/TLS certificate to GitLab
gitlab-pages certificate add ghastbot.gitlab.io ghastbot.gitlab.io.crt ghastbot.gitlab.io.key

# Create a `.well-known/acme-challenge` directory in your GitLab project
mkdir -p .well-known/acme-challenge

# Place your SSL/TLS certificate files in the `.well-known/acme-challenge` directory
cp ghastbot.gitlab.io.crt .well-known/acme-challenge/ghastbot.gitlab.io.crt
cp ghastbot.gitlab.io.key .well-known/acme-challenge/ghastbot.gitlab.io.key

# Commit and push your changes to GitLab
git add .well-known/acme-challenge
git commit -m "Add SSL/TLS certificate"
git push
